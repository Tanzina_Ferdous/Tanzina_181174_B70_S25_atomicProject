<?php
namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database;
use PDO;

class ProfilePicture extends Database
{
     public $id, $name, $profilePicture;

     
     public function setData($postArray){

         if(array_key_exists("id",$postArray))
             $this->id = $postArray["id"];

         if(array_key_exists("Name",$postArray))
             $this->name = $postArray["Name"];
         
         if(array_key_exists("ProfilePicture",$postArray))
               $this->profilePicture = $postArray["ProfilePicture"];
         

     }// end of setData Method


    public function store(){

        $sqlQuery = "INSERT INTO profile_picture ( name, profile_picture) VALUES ( ?, ?)";
        $sth = $this->dbh->prepare( $sqlQuery );
      
      
        $dataArray = [ $this->name, $this->profilePicture ];
    
        $status = $sth->execute($dataArray);

        if($status)
    
            Message::message("Success! Data has been inserted successfully<br>");
        else
            Message::message("Failed! Data has not been inserted<br>");

    }// end of store() Method
    
    
    
    
    
    
    
    public function index(){
        
          $sqlQuery = "SELECT * FROM profile_picture  WHERE is_trashed='NO'";
        
          $sth = $this->dbh->query($sqlQuery);
           
          $sth->setFetchMode(PDO::FETCH_OBJ);
        
          $allData=  $sth->fetchAll();
          return $allData;
        
    }// end of index() Method






    public function view(){

        $sqlQuery = "SELECT * FROM profile_picture WHERE id=".$this->id;

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData=  $sth->fetch();
        return $oneData;

    }// end of index() Method






    public function update(){

        $sqlQuery = "UPDATE profile_picture SET name=?, profile_picture=? WHERE id=".$this->id;

        $sth = $this->dbh->prepare( $sqlQuery );


        $dataArray = [ $this->name, $this->profilePicture ];

        $status = $sth->execute($dataArray);

        if($status)

            Message::message("Success! Data has been updated successfully<br>");
        else
            Message::message("Failed! Data has not been updated<br>");

    }// end of store() Method




    public function delete(){

        $sqlQuery = "DELETE FROM profile_picture where id=".$this->id;

        $status = $this->dbh->exec($sqlQuery);


        if($status)

            Message::message("Success! Data has been deleted successfully<br>");
        else
            Message::message("Failed! Data has not been deleted<br>");


    }//end of delete() Method




    public function trash(){


        $sqlQuery = "UPDATE profile_picture SET is_trashed=NOW() WHERE id=".$this->id;


        $status = $this->dbh->exec($sqlQuery);

        if($status)

            Message::message("Success! Data has been trashed successfully<br>");
        else
            Message::message("Failed! Data has not been trashed<br>");



    }// end of trash()




    public function trashed(){

        $sqlQuery = "SELECT * FROM profile_picture  WHERE is_trashed<>'NO'";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;

    }// end of trashed() Method





    public function recover(){


        $sqlQuery = "UPDATE profile_picture SET is_trashed='NO' WHERE id=".$this->id;


        $status = $this->dbh->exec($sqlQuery);

        if($status)

            Message::message("Success! Data has been recovered successfully<br>");
        else
            Message::message("Failed! Data has not been recovered<br>");



    }// end of recover()






}// end of ProfilePicture Class