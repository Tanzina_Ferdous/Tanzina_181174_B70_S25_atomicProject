<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;
use PDO;

class BookTitle extends Database
{
    public $id, $bookTitle, $authorName;
    
    public function setData($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray['id'];

        if(array_key_exists("BookTitle",$postArray))
              $this->bookTitle = $postArray['BookTitle'];

        if(array_key_exists("AuthorName",$postArray))
              $this->authorName = $postArray['AuthorName'];

    }// end of setData() Method



    public function store(){


        $sqlQuery = "INSERT INTO book_title (book_title, author_name) VALUES (? , ?)";
        $dataArray = [ $this->bookTitle, $this->authorName ];

        $sth =  $this->dbh->prepare($sqlQuery);

        $status =  $sth->execute($dataArray);


        if($status)
            Message::setMessage("Success! Data has been inserted successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been inserted. <br>");


    }// end of store() Method




    public function index(){

        $sqlQuery = "Select * from book_title WHERE is_trashed='NO'";


        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }





    public function view(){

        $sqlQuery = "Select * from book_title where id=".$this->id;



        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData =  $sth->fetch();

        return $oneData;
    }


    public function update(){

        $sqlQuery  = "UPDATE book_title SET book_title=?, author_name = ? WHERE id=".$this->id;



        $dataArray = [$this->bookTitle, $this->authorName];


        $sth = $this->dbh->prepare($sqlQuery);

        $status =  $sth->execute($dataArray);

        if($status)
            Message::setMessage("Success! Data has been updated successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been updated. <br>");


    }

   

    public function trash(){

        $sqlQuery = "UPDATE book_title SET is_trashed=NOW() WHERE id= ".$this->id;

         $this->dbh->exec($sqlQuery);
        
    }

    public function trashed(){

        $sqlQuery = "Select * from book_title WHERE  is_trashed <> 'NO'";


        $sth =  $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function recover(){

        $sqlQuery = "UPDATE book_title SET is_trashed='NO' WHERE id=".$this->id;

        $this->dbh->exec($sqlQuery);
    }

    public function delete(){

        $sqlQuery = "DELETE FROM book_title  WHERE id=".$this->id;

        $this->dbh->exec($sqlQuery);
    }

}// end of BookTitle Class














