<?php

require_once  ("../../../vendor/autoload.php");

if (!isset($_SESSION)) session_start();

use App\BookTitle\BookTitle;
use App\Utility\Utility;

$obj = new BookTitle();

$obj->setData($_GET);

$obj->recover();

Utility::redirect("trashed.php");

?>